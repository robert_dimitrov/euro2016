<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__.'/php-console-master/src/PhpConsole/__autoload.php');
//$connector = PhpConsole\Connector::getInstance();
//$handler = PhpConsole\Handler::getInstance();

//include $_SERVER['DOCUMENT_ROOT'].'/mme/includes/dbconnect.php';
include 'dbconnect.php';

             try{
                  $username = $_POST['username'];
                  $password = $_POST['password'];

                  $userexists = $pdo->prepare("SELECT COUNT(*) FROM User WHERE username = :username");
                  $userexists->bindValue(":username", $username);
                  $userexists->execute();
                  $exists = $userexists->fetchColumn();
                  if($exists < 1){
                    $error = "False username";
                    header('Location:../login.php?err='.urlencode($error));
                    exit();
                  }

                  $passwordValid = $pdo->prepare("SELECT * FROM User where username = :username");
                  $passwordValid->bindValue(":username", $username);
                  $passwordValid->execute();
                  $user = $passwordValid->fetch();

                  if (!password_verify($password, $user['password'])) {
                      $error = "False password";
                      header('Location:../login.php?err='.urlencode($error));
                      exit();
                  } else {

                  $_SESSION['username'] = $username;
                  if($username === 'adminrobert'){
                    $_SESSION['isadmin'] = true;
                  }

                  header("Location: ../standings.php");

                  }

              }catch (PDOException $e){
                header("Location: ../login.php?err=".$e->getMessage());
            }


?>