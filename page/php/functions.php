<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__.'/php-console-master/src/PhpConsole/__autoload.php');
//$connector = PhpConsole\Connector::getInstance();
//$handler = PhpConsole\Handler::getInstance();

//include $_SERVER['DOCUMENT_ROOT'].'/mme/includes/dbconnect.php';
include 'dbconnect.php';

             try{
                  $invitValid = $pdo->prepare('SELECT COUNT(*) FROM User WHERE invitation_code = :invitation');
                  $invitValid->bindValue(":invitation", $_POST['invitation']);
                  $invitValid->execute();
                  $valid = $invitValid->fetchColumn();
                  if($valid < 1){
                    $error = "False invitation code";
                    header('Location:../signup.php?err='.urlencode($error));
                    exit();
                  }

                  $invitUsed = $pdo->prepare("SELECT username FROM User WHERE invitation_code = :invitation");
                  $invitUsed->bindValue(":invitation", $_POST['invitation']);
                  $invitUsed->execute();
                  $used = $invitUsed->fetchColumn();
                  if($used !== NULL){
                    $error = "Invitation code already used".$_POST['invitation'];
                    header('Location:../signup.php?err='.urlencode($error));
                    exit();
                  }

                  $nameUsed = $pdo->prepare("SELECT COUNT(*) FROM User WHERE username = :username");
                  $nameUsed->bindValue(":username", $_POST['username']);
                  $nameUsed->execute();
                  $count = $nameUsed->fetchColumn();
                  if($count > 0){
                     $error = "Username already used: ";
                     header('Location:../signup.php?err='.urlencode($error));
                     exit();
                  }

                  $nameUsed = $pdo->prepare("SELECT COUNT(*) FROM User WHERE display_name = :display_name");
                    $nameUsed->bindValue(":display_name", $_POST['display_name']);
                    $nameUsed->execute();
                    $count = $nameUsed->fetchColumn();
                    if($count > 0){
                       $error = "Display Name already used";
                       header('Location:../signup.php?err='.urlencode($error));
                       exit();
                    }


                  $username = $_POST['username'];
                  $display_name = $_POST['display_name'];
                  $invitation = $_POST['invitation'];
                  $password = $_POST['password'];
                  $hash = password_hash($password, PASSWORD_DEFAULT);

                  $insert = $pdo->prepare("UPDATE User SET username = :username, password = :password, display_name = :display_name, points = 0, perfect = 0
                  WHERE invitation_code = :invitation");
                  $insert->bindValue(":username", $username);
                  $insert->bindValue(":password", $hash);
                  $insert->bindValue(":display_name", $display_name);
                  $insert->bindValue(":invitation", $invitation);
                  $insert->execute();

                  $_SESSION['username'] = $username;


                  header("Location: ../index.php");

              }catch (PDOException $e){
                  header("Location: ../signup.php?err=".$e->getMessage());
              }


?>