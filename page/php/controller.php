<?php

session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__.'/php-console-master/src/PhpConsole/__autoload.php');
//$connector = PhpConsole\Connector::getInstance();
//$handler = PhpConsole\Handler::getInstance();

//include $_SERVER['DOCUMENT_ROOT'].'/mme/includes/dbconnect.php';
include 'dbconnect.php';

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    switch($_POST['action']){
        case 'placeBet':
                    $response = array("message" => "Error while placing a bet.");
                    $gdt = null;
                    try{
                        $timeStm = $pdo->prepare('SELECT date FROM Game WHERE gameid = :game_id');
                        $timeStm->bindValue(":game_id", $_POST['game_id']);
                        $timeStm->execute();
                        $gamedate = $timeStm->fetchColumn();
                        $gameTimestamp = strtotime($gamedate);
                        $nowTimestamp = time();
                        $timeDifference = $gameTimestamp - $nowTimestamp;

                        if($timeDifference > 600){

                        $test = $pdo->prepare('SELECT COUNT(*) FROM Bet WHERE username=:username AND game_id = :game_id');
                        $test->bindValue(":username", $_SESSION['username']);
                        $test->bindValue(":game_id", $_POST['game_id']);
                        $test->execute();
                        $count = $test->fetchColumn();
                        $message = array("count" => $count);

                        if($count == "0"){
                            $sql = 'INSERT INTO Bet SET
                                    game_id = :game_id,
                                    username = :username,
                                    date = :date,
                                    bet_outcome = :outcome,
                                    bet_result = :result';
                                    $s = $pdo->prepare($sql);
                                    $s->bindValue(':game_id', $_POST['game_id']);
                                    $s->bindValue(':username', $_SESSION['username']);
                                    $s->bindValue(':date', date("Y-m-d H:i:s"));
                                    $s->bindValue(':outcome', $_POST['outcome']);
                                    $s->bindValue(':result', $_POST['result']);
                                    $s->execute();
                                    $response = array("message" => "Success");
                                    $response = array("gameid" => $_POST['game_id']);
                        } else {
                            $sql = 'UPDATE Bet SET
                                    game_id = :game_id,
                                    username = :username,
                                    date = :date,
                                    bet_outcome = :outcome,
                                    bet_result = :result
                                    WHERE game_id = :game_id AND username = :username';
                                    $s = $pdo->prepare($sql);
                                    $s->bindValue(':game_id', $_POST['game_id']);
                                    $s->bindValue(':username', $_SESSION['username']);
                                    $s->bindValue(':date', date("Y-m-d H:i:s"));
                                    $s->bindValue(':outcome', $_POST['outcome']);
                                    $s->bindValue(':result', $_POST['result']);
                                    $s->execute();
                                    $response = array("message" => "Success");
                                    $response = array("gameid" => $_POST['game_id']);
                        }
                        }

                    } catch (PDOException $e){
                        $response = array("databaseError" => "Fehler");
                        $json = json_encode($response);
                        echo $json;
                        exit();
                    }
                    $json = json_encode($response);
                    echo $json;
                    exit();
                    break;

         case 'addResult':
                    $response = null;

                        try{
                            $sql = "UPDATE Game SET
                                    outcome = :outcome,
                                    result = :result
                                    WHERE gameid = :game_id";
                            $s = $pdo->prepare($sql);
                            $s->bindValue(':outcome', $_POST['outcome']);
                            $s->bindValue(':result', $_POST['result']);
                            $s->bindValue(':game_id', $_POST['game_id']);
                            $s->execute();
                            $response = array("message" => "Success");

                            // updating the 'points' column in BET
                            $stm = "SELECT * FROM Bet WHERE game_id = :game_id";
                            $s = $pdo->prepare($stm);
                            $s->bindValue(":game_id", $_POST['game_id']);
                            $s->execute();

                            $result = $s->fetchAll();

                            foreach($result as $row){
                                $points = 0;
                                if($row['bet_outcome'] === $_POST['outcome']){
                                    $points = 1;
                                    if($row['bet_result'] === $_POST['result']){
                                        $points = 3;
                                    }
                                }
                                $update = "UPDATE Bet SET
                                points = :points
                                WHERE bet_id = :bet_id";
                                $statement = $pdo->prepare($update);
                                $statement->bindValue(":points", $points);
                                $statement->bindValue(":bet_id", $row['bet_id']);
                                $statement->execute();
                            }

                            //updating the users, wish me luck!
                            $sqlUsers = $pdo->prepare("SELECT * FROM User");
                            $sqlUsers->execute();

                            $users = $sqlUsers->fetchAll();

                            foreach($users as $user){
                                $sqlBets = "SELECT * FROM Bet WHERE username = :username";
                                $s = $pdo->prepare($sqlBets);
                                $s->bindValue(':username', $user['username']);
                                $s->execute();

                                $points = 0;
                                $perfect = 0;

                                $bets = $s->fetchAll();
                                foreach($bets as $bet){
                                    $rowPoints = $bet['points'];
                                    $points = $points + $rowPoints;
                                    if($rowPoints == 3){
                                        $perfect = $perfect + 1;
                                    }
                                }

                                $sqlUpdate = "UPDATE User SET
                                points = :points,
                                perfect = :perfect
                                WHERE username = :username";
                                $s = $pdo->prepare($sqlUpdate);
                                $s->bindValue(":points", $points);
                                $s->bindValue(":perfect", $perfect);
                                $s->bindValue(":username", $user['username']);
                                $s->execute();
                            }

                        } catch (PDOException $e){
                            $response = array("databaseError" => "Fehler");
                            $json = json_encode($response);
                            echo $json;
                            exit();
                        }
                        $json = json_encode($response);
                        echo $json;
                        exit();
                        break;

    }
} elseif($_SERVER['REQUEST_METHOD'] === 'GET'){
    switch($_GET['action']){
        case 'getActiveBets':
            $response = null;
            try{
                $statement = $pdo->prepare('SELECT * from Game g
                                            LEFT JOIN (SELECT game_id, bet_result, username from Bet where Bet.username = :username AND Bet.username IS NOT NULL) b
                                            ON g.gameid = b.game_id
                                            WHERE DATE_ADD(NOW(), INTERVAL 10 MINUTE) < g.date');
                $statement->bindValue(":username", $_SESSION['username']);
                $statement->execute();
            } catch(PDOException $e){
                $error = 'An error while loading all active bets: '.$e->getMessage();
                $response = array("databaseError" => $error);
                $json = json_encode($response);
                echo $json;
                exit();
            }

            $result = $statement->fetchAll();

            $games = array();

            foreach($result as $row){
                	$homeFK = $row['home_id'];
                	$homeTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :homeid');
                	$homeTeamStatement->bindValue(':homeid', $homeFK);
                	$homeTeamStatement->execute();
                	$home = $homeTeamStatement->fetch();

                	$awayFK = $row['away_id'];
                	$awayTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :awayid');
                	$awayTeamStatement->bindValue(':awayid', $awayFK);
                	$awayTeamStatement->execute();
                	$away = $awayTeamStatement->fetch();

                	$games[] = array("date" => $row['date'],
                	"home_name" => $home['name'],
                	"home_alpha2" => $home['alpha_2'],
                	"away_name" => $away['name'],
                	"away_alpha2" => $away['alpha_2'],
                	"bet" => $row['bet_result'],
                	"id" => $row['gameid']);
            }
            $json = json_encode($games);
            echo $json;
            exit();
            break;

         case 'getOldBets':
            $response = null;
                        try{
                            $statement = $pdo->prepare('SELECT * from Game g WHERE result IS NOT NULL');
                            $statement->bindValue(":username", $_SESSION['username']);
                            $statement->execute();
                        } catch(PDOException $e){
                            $error = 'An error while loading the old bets: '.$e->getMessage();
                            $response = array("databaseError" => $error);
                            $json = json_encode($response);
                            echo $json;
                            exit();
                        }

                        $result = $statement->fetchAll();

                        $oldBets = array();

                        foreach($result as $row){
                            	$homeFK = $row['home_id'];
                            	$homeTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :homeid');
                            	$homeTeamStatement->bindValue(':homeid', $homeFK);
                            	$homeTeamStatement->execute();
                            	$home = $homeTeamStatement->fetch();

                            	$awayFK = $row['away_id'];
                            	$awayTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :awayid');
                            	$awayTeamStatement->bindValue(':awayid', $awayFK);
                            	$awayTeamStatement->execute();
                            	$away = $awayTeamStatement->fetch();

                            	$oldBets[] = array("date" => $row['date'],
                            	"id" => $row['gameid'],
                            	"home_name" => $home['name'],
                            	"home_alpha2" => $home['alpha_2'],
                            	"away_name" => $away['name'],
                            	"away_alpha2" => $away['alpha_2'],
                            	"result" =>$row['result']);
                        }
                        $json = json_encode($oldBets);
                        echo $json;
                        exit();
                        break;
        case 'getPlacedBets':
                    $response = null;
                                try{
                                    $statement = $pdo->prepare("SELECT * from Game WHERE NOW() > DATE_SUB(date, INTERVAL 10 MINUTE) AND NOW() < DATE_ADD(date, INTERVAL 110 MINUTE)");
                                    $statement->execute();
                                } catch(PDOException $e){
                                    $error = 'An error while loading the placed bets: '.$e->getMessage();
                                    $response = array("databaseError" => $error);
                                    $json = json_encode($response);
                                    echo $json;
                                    exit();
                                }

                                $result = $statement->fetchAll();

                                $placedBets = array();

                                foreach($result as $row){
                                    	$homeFK = $row['home_id'];
                                    	$homeTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :homeid');
                                    	$homeTeamStatement->bindValue(':homeid', $homeFK);
                                    	$homeTeamStatement->execute();
                                    	$home = $homeTeamStatement->fetch();

                                    	$awayFK = $row['away_id'];
                                    	$awayTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :awayid');
                                    	$awayTeamStatement->bindValue(':awayid', $awayFK);
                                    	$awayTeamStatement->execute();
                                    	$away = $awayTeamStatement->fetch();
//
                                    	$placedBets[] = array("date" => $row['date'],
                                    	"id" => $row['gameid'],
                                    	"home_name" => $home['name'],
                                    	"home_alpha2" => $home['alpha_2'],
                                    	"away_name" => $away['name'],
                                    	"away_alpha2" => $away['alpha_2']);

                                }
                                $json = json_encode($placedBets);
                                echo $json;
                                exit();
                                break;
        case 'getResults':
                            $response = null;
                                        try{
                                            $statement = $pdo->prepare('SELECT * FROM Game WHERE result IS NOT NULL order by gameid desc ');
                                            $statement->execute();
                                        } catch(PDOException $e){
                                            $error = 'An error while loading the results: '.$e->getMessage();
                                            $response = array("databaseError" => $error);
                                            $json = json_encode($response);
                                            echo $json;
                                            exit();
                                        }

                                        $result = $statement->fetchAll();

                                        $oldBets = array();

                                        foreach($result as $row){
                                            	$homeFK = $row['home_id'];
                                            	$homeTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :homeid');
                                            	$homeTeamStatement->bindValue(':homeid', $homeFK);
                                            	$homeTeamStatement->execute();
                                            	$home = $homeTeamStatement->fetch();

                                            	$awayFK = $row['away_id'];
                                            	$awayTeamStatement = $pdo->prepare('SELECT * from TEAM where team_id = :awayid');
                                            	$awayTeamStatement->bindValue(':awayid', $awayFK);
                                            	$awayTeamStatement->execute();
                                            	$away = $awayTeamStatement->fetch();

                                            	$oldBets[] = array("date" => $row['date'],
                                            	"id" => $row['gameid'],
                                            	"home_name" => $home['name'],
                                            	"home_alpha2" => $home['alpha_2'],
                                            	"away_name" => $away['name'],
                                            	"away_alpha2" => $away['alpha_2'],
                                            	"result" =>$row['result']);
                                        }
                                        $json = json_encode($oldBets);
                                        echo $json;
                                        exit();
                                        break;
        case 'getResultsToBet':
                                    $response = null;
                                                try{
                                                    $statement = $pdo->prepare('SELECT * FROM Bet JOIN (SELECT username, display_name FROM User) U USING (username) WHERE Bet.game_id = :game_id');
                                                    $statement->bindValue(':game_id', $_GET['game_id']);
                                                    $statement->execute();
                                                } catch(PDOException $e){
                                                    $error = 'An error while loading the results: '.$e->getMessage();
                                                    $response = array("databaseError" => $error);
                                                    $json = json_encode($response);
                                                    echo $json;
                                                    exit();
                                                }

                                                $result = $statement->fetchAll();

                                                $oldBets = array();

                                                foreach($result as $row){
                                                    	$oldBets[] = array("username" => $row['username'],
                                                    	"display_name" => $row['display_name'],
                                                    	"bet_result" => $row['bet_result'],
                                                    	"points" => $row['points']);
                                                }
                                                $json = json_encode($oldBets);
                                                echo $json;
                                                exit();
                                                break;
        case 'getStandings':
                                    $response = null;
                                                try{
                                                    $statement = $pdo->prepare("SELECT * FROM User WHERE points IS NOT NULL AND username != 'adminrobert'");
                                                    $statement->execute();
                                                } catch(PDOException $e){
                                                    $error = 'An error while loading the standings: '.$e->getMessage();
                                                    $response = array("databaseError" => $error);
                                                    $json = json_encode($response);
                                                    echo $json;
                                                    exit();
                                                }

                                                $result = $statement->fetchAll();

                                                $oldBets = array();

                                                foreach($result as $row){
                                                    	$stm = $pdo->prepare('SELECT COUNT(*) FROM Bet WHERE username = :username AND points IS NOT NULL');
                                                    	$stm->bindValue(':username', $row['username']);
                                                    	$stm->execute();
                                                    	$count = $stm->fetchColumn();

                                                    	$oldBets[] = array("name" => $row['display_name'],
                                                    	"count" => $count,
                                                    	"good" => ($row['points']-$row['perfect']*3),
                                                    	"points" => $row['points'],
                                                    	"perfect" => $row['perfect']);
                                                }
                                                $json = json_encode($oldBets);
                                                echo $json;
                                                exit();
                                                break;

        case 'getTeams':
                                            $response = null;
                                                        try{
                                                            $homeStatement = $pdo->prepare('SELECT * FROM TEAM WHERE name = :home');
                                                            $homeStatement->bindValue(':home', $_GET['home']);
                                                            $homeStatement->execute();

                                                            $home = $homeStatement->fetch();

                                                            $awayStatement = $pdo->prepare('SELECT * FROM TEAM WHERE name = :away');
                                                            $awayStatement->bindValue(':away', $_GET['away']);
                                                            $awayStatement->execute();

                                                            $away = $awayStatement->fetch();

                                                            $teamInfo = array();

                                                            $teamInfo[] = array("home_name" => $home['name'],
                                                            "home_bench" => $home['bench'],
                                                            "home_power" => $home['power'],
                                                            "home_primary" => $home["primary_color"],
                                                            "home_secondary" => $home['secondary_color'],
                                                            "home_alpha" => $home['alpha_2'],
                                                            "home_lineup" => $home['lineup']);

                                                            $teamInfo[] = array("away_name" => $away['name'],
                                                            "away_bench" => $away['bench'],
                                                            "away_power" => $away['power'],
                                                            "away_primary" => $away["primary_color"],
                                                            "away_secondary" => $away['secondary_color'],
                                                            "away_alpha" => $away['alpha_2'],
                                                            "away_lineup" => $away['lineup']);

                                                            $teamInfo[] = array("time" => $_GET['time']);

                                                           $json = json_encode($teamInfo);
                                                           echo $json;
                                                           exit();

                                                        } catch(PDOException $e){
                                                            $error = 'An error while loading the results: '.$e->getMessage();
                                                            $response = array("databaseError" => $error);
                                                            $json = json_encode($response);
                                                            echo $json;
                                                            exit();
                                                        }
                                                         break;


    }
}

?>