<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

//define('__ROOT__', dirname(dirname(__FILE__)));
//require_once(__ROOT__.'/php-console-master/src/PhpConsole/__autoload.php');
//$connector = PhpConsole\Connector::getInstance();
//$handler = PhpConsole\Handler::getInstance();

//include $_SERVER['DOCUMENT_ROOT'].'/mme/includes/dbconnect.php';

session_unset();
session_destroy();

header("Location: ../index.php");
exit();

?>