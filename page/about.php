<?php
    session_start();
?>


<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Results" ?>
<?php include 'head.php'?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img style="width: 80%; margin:auto" src="flags/ca_welcome.png" alt="Welcome">
    </div>

    <div class="item">
      <img style="width: 80%; margin:auto" src="flags/ca_cup.png" alt="Cup">
    </div>

    <div class="item">
      <img style="width: 80%; margin:auto" src="flags/ca_machine.png" alt="Machine">
    </div>


  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<a href="simulator.php">
<button class="btn btn-primary continuebutton">Continue</button>
</a>








<?php include 'scripts.php'?>
</body>
</html>