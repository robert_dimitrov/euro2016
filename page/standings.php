<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Standings" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" id="standingspage">
                        <h1 class="page-header">
                            Standings
                        </h1>
                    </div>
                </div>

                <?php
                if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                 echo '<!-- /.row -->
                                       <br>
                                       <div class="row">
                                       <div class="col-lg-3"></div>
                                           <div class="col-lg-6" id="standings">

                                           </div>
                                       </div>';
                } else {
                  echo '<div class="row" id="alert-active">
                      <div class="col-lg-12">
                          <div class="alert alert-danger alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <i class="fa fa-info-circle"></i>  In order to see the standings, you need to be logged-in.
                          </div>
                      </div>';
                }
                ?>





            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>



<?php include 'scripts.php'?>
<script src="js/standings.js"></script>
</body>
</html>