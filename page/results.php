<?php
    session_start();
?>


<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Results" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Results
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <?php
                    if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {

                        echo "<div id='results-content'></div>";

                        if(isset($_SESSION['isadmin']) && !empty($_SESSION['isadmin'])){
                             echo '<div class="row">
                                                       <div class="col-lg-12">
                                                           Add a result:
                                                           <input type="text" id="result-game-id" placeholder="game id">
                                                           <input type="text" id="result-result" placeholder="result">
                                                           <button type="button" class="btn btn-primary" id="add-result">Add</button>
                                                       </div>
                                                   </div>';
                        }

                    } else {
                        echo '<div class="row" id="alert-active">
                                              <div class="col-lg-12">
                                                  <div class="alert alert-danger alert-dismissable">
                                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                      <i class="fa fa-info-circle"></i>  In order to see the results, you need to be logged-in.
                                                  </div>
                                              </div>';
                    }
                ?>




                    <div id="results-content"></div>
                    <br>


                <!-- /.row -->
                </div>
                <!-- /.row -->



            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>








<?php include 'scripts.php'?>
<script src="js/results.js"></script>
</body>
</html>