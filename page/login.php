<?php
 session_start();
?>


<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Log In" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" id="standingspage">
                        <h1 class="page-header">
                            Log-In
                        </h1>
                    </div>
                </div>

                <?php
                if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                 echo '<div class="row" id="alert-active">
                                       <div class="col-lg-12">
                                           <div class="alert alert-success alert-dismissable">
                                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                               <i class="fa fa-info-circle"></i>  You are already signed up and logged in, baby ;)
                                           </div>
                                       </div>';
                } else {
                  $input = '<form id="signupform" class="form-inline" role="form" action="php/login-controller.php" method="POST">
                              <div class="form-group">
                                  <label class="login-label" for="email">Username:</label>
                                  <input type="text" class="form-control" name="username" id="username" pattern="[a-zA-Z0-9_]{4,30}">
                                </div><br><br>
                              <div class="form-group">
                                <label class="login-label" for="pwd">Password:</label>
                                <input type="password" class="form-control" name="password" id="password" pattern=".{4,30}">
                              </div>
                              <span class="forgot" data-toggle="tooltip" title="Please contact me" data-placement="right"> Forgot password?</span><br><br>
                              <input id="signupbutton" type="submit" class="btn btn-success" disabled></button>
                            </form><br>';
                  echo $input;
                  if($_GET){
                    echo '<div class="db-error" id="error-message"><p>';
                    echo $_GET['err'];
                    echo '</p></div>';
                  }
                }
                ?>





            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>



<?php include 'scripts.php'?>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<script src="js/login.js"></script>
</body>
</html>