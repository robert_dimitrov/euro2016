<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="flags/logo1.png" id="logo" style="width: 50px; display: inline"><span class="logo-name"> EM-BET 2016</span></a>
            </div>
            <!-- Top Menu Items -->
            <?php
            if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
            $username = $_SESSION['username'];
            echo '<ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ';
                    echo $username;
                    echo '<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
                        <li class="divider"></li>
                        <li>
                            <a href="php/logout-controller.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>';
            } else {
                echo '<ul class="nav navbar-right top-nav">
                                      <li>
                                          <a href="login.php"><i class="fa fa-keyboard-o"></i> Login</a>
                                      </li>
                                      <li>
                                        <a href="signup.php"><i class="fa fa-users"></i> Sign-Up</a>
                                      </li>
                                  </ul>';
            }
            ?>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-futbol-o"></i> Bets</a>
                    </li>
                    <li>
                        <a href="results.php"><i class="fa fa-fw fa-list"></i> Results</a>
                    </li>
                    <li>
                        <a href="standings.php"><i class="fa fa-fw fa-trophy"></i> Standings</a>
                    </li>
                    <li>
                        <a href="simulator.php"><i class="fa fa-fw fa-random"></i> Simulator / THE MACHINE™</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

