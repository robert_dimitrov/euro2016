$(".form-control").keyup(function(){
    var username = $("#username");
    var password = $("#password");
    var invitation = $("#invitation");
    var display_name = $("#display_name");

    var empty = invitation.val() === "" || username.val() === "" || password.val() === "" || display_name.val() === "";

    var valid = username.val().match(username.attr("pattern")) &&
        password.val().match(password.attr("pattern")) && invitation.val().match(invitation.attr("pattern")) && display_name.val().match(display_name.attr("pattern"));
    $("#signupbutton").attr("disabled", !valid || empty);
});