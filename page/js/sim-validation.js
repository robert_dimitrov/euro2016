var Team = function(name, players, bench, power, primaryColor, secondaryColor){
    this.name = name;
    this.players = players;
    this.bench = bench;
    this.power = power;
    if(!primaryColor){
        primaryColor = "#444444";
    }
    this.primaryColor = primaryColor;
    this.secondaryColor = secondaryColor || "#cccccc";

    this.cautioned = [];
    this.sentoff = [-1];
    this.cameIn = [-1];

    this.substitute = function(){
        var playerIndex = Math.floor(Math.random()*(this.players.length-1)+1);
        while(this.sentoff.indexOf(playerIndex) >= 0){
            playerIndex = Math.floor(Math.random()*(this.players.length-1)+1);
        }
        var benchIndex = Math.round(Math.floor(Math.random()*(this.bench.length-1)));
        while(this.cameIn.indexOf(benchIndex) >= 0){
            benchIndex = Math.round(Math.floor(Math.random()*(this.bench.length-1)));
        }
        this.cameIn.push(benchIndex);
        var playerOff = this.players[playerIndex];
        var playerIn = this.bench[benchIndex];
        this.players[playerIndex] = playerIn;
        // delete this.bench[benchIndex];
        // this.sentoff.push(playerIndex);
        return [playerOff, playerIn];
    };

    this.getScorer = function(){
        var count = 0;
        var chance = 0;
        var possibleScorers = [];
        var sent = this.sentoff;

        var playerIndex = 0;
        this.players.forEach(function(p){
                if(sent.indexOf(playerIndex) < 0){
                    if(count > 0) chance = 1;
                    if(count > 4) chance = 2;
                    if(count > 8) chance = 5;
                    for(var i = 0; i < chance; i++){
                        possibleScorers.push(p);
                    }
                }
                count++;
                playerIndex++;
        });
        return possibleScorers[Math.floor(Math.random()*possibleScorers.length)];
    };

    this.markYellow = function(){
        var playerIndex;
        while(true){
            playerIndex = Math.floor(Math.random()*(this.players.length-1))+1;
            if(this.sentoff.indexOf(playerIndex) < 0) break;
        }
        var player = this.players[playerIndex];
        if(this.cautioned.indexOf(player) >= 0){
            this.sentoff.push(playerIndex);
            return [player, "red"];
        }
        this.cautioned.push(player);
        return [player, "yellow"];
    };

    this.markRed = function(){
        var playerIndex;
        while(true){
            playerIndex = Math.floor(Math.random()*(this.players.length-1))+1;
            if(this.sentoff.indexOf(playerIndex) < 0) break;
        }
        var player = this.players[playerIndex];
        delete this.players[playerIndex];
        this.sentoff.push(playerIndex);
        return [player, "red"];
    };

};

var Game = function(home, away){
    this.home = home;
    this.away = away;
    this.teams = [home, away];

    this.result = [0,0];
    this.possessionParts = [0,0];
    this.shots = [0,0];
    this.corners = [0,0];
    this.chances = [0,0];

};

var StateSimulator = function(game){
    this.game = game;
    this.chances = [];
    this.chances[0] = calculateChances()[0];
    this.chances[1] = calculateChances()[1];

    function calculateChances(){
        var sum = game.home.power + game.away.power;
        return [Math.round(game.home.power*10/sum), Math.round(game.away.power*10/sum)];
    }

    function State(name, team, chance){
        this.name = name;
        this.team = team;
        this.chance = chance;
    }

    this.getStage = function(name, team){
        var stage = {};
        stage.name = name;
        stage.team = team;

        switch(name){
            case "Control":
                stage.next =   [new State("Attack", team, this.chances[team]*6),
                    new State("Fouled", team, 2),
                    new State("Attack", 1-team, this.chances[1-team]),
                    new State("Fouled", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Attack":
                stage.next =   [new State("Control", team, 5),
                    new State("Shot", team, this.chances[team]*3),
                    new State("Header", team, this.chances[team]),
                    new State("Cross", team, this.chances[team]*2),
                    new State("Offside", team, 2),
                    new State("Win ball", 1-team, this.chances[1-team]),
                    new State("Free kick", team, 7),
                    new State("Fouled", team, 3),
                    new State("Fouled", 1-team, 1),
                    new State("Clearance", 1-team, this.chances[1-team])];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Counter attack":
                stage.next =   [new State("Penalty", team, 2),
                    new State("Shot", team, this.chances[team]*3),
                    new State("Header", team, this.chances[team]),
                    new State("Win ball", 1-team, this.chances[1-team]),
                    new State("Attack", team, 4),
                    new State("Cross", team, this.chances[team]*2),
                    new State("Offside", team, 5),
                    new State("Free kick", team, 7),
                    new State("Clearance", 1-team, this.chances[1-team]*2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Shot":
                stage.next =   [new State("Block", 1-team, 13),
                    new State("Save", 1-team, 18),
                    new State("Corner", team, 15),
                    new State("Out", team, 16),
                    new State("Goal", team, this.chances[team]),
                    new State("Hand", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
            case "Header":
                stage.next =   [new State("Block", 1-team, 3),
                    new State("Save", 1-team, 25),
                    new State("Corner", team, 25),
                    new State("Out", team, 23),
                    new State("Goal", team, this.chances[team]),
                    new State("Hand", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
            case "Corner":
                stage.next =   [new State("Shot", team, this.chances[team]),
                    new State("Header", team, this.chances[team]),
                    new State("Clearance", 1-team, this.chances[1-team]*2),
                    new State("Penalty", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.corners[team]++;
                };
                break;
            case "Win ball":
                stage.next =   [new State("Attack", team, 2),
                    new State("Counter attack", team, 3),
                    new State("Control", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Clearance":
                stage.next =   [new State("Attack", 1-team, this.chances[1-team]),
                    new State("Control", 1-team, this.chances[1-team]),
                    new State("Counter attack", team, this.chances[team]),
                    new State("Attack", team, this.chances[team])];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Block":
                stage.next =   [new State("Attack", 1-team, this.chances[1-team]*2),
                    new State("Attack", team, this.chances[team]*3),
                    new State("Counter attack", team, this.chances[team]*2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Save":
                stage.next =   [new State("Attack", team, 4),
                    new State("Counter attack", team, this.chances[team]*2),
                    new State("Control", team, 3)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Hand":
                stage.next =   [new State("Penalty", 1-team, 1)];stage.effect = function(){
                this.possessionParts[1-team]++;
            };
                break;
            case "Goal":
                stage.next =   [new State("Control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.result[team]++;
                };
                break;
            case "Out":
                stage.next =   [new State("Attack", 1-team, 3),
                    new State("Control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[1-team]++;
                };
                break;
            case "Offside":
                stage.next =   [new State("Attack", 1-team, 3),
                    new State("Control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[1-team]++;
                };
                break;
            case "Fouled":
                stage.next =   [new State("Attack", team, 3),
                    new State("Control", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Free kick":
                stage.next =   [new State("Cross", team, 4),
                    new State("Goal", team, this.chances[team]),
                    new State("Block", 1-team, this.chances[1-team]),
                    new State("Out", team, 12),
                    new State("Penalty", team, 2),
                    new State("Corner", team, 11)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
            case "Cross":
                stage.next =   [new State("Offside", team, 7),
                    new State("Penalty", team, 1),
                    new State("Fouled", 1-team, 3),
                    new State("Win ball", 1-team, this.chances[1-team]*2),
                    new State("Clearance", 1-team, this.chances[1-team]*2),
                    new State("Shot", team, this.chances[team]),
                    new State("Header", team, this.chances[team])];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "Penalty":
                stage.next =   [new State("Goal", team, 21),
                    new State("Save", 1-team, 3),
                    new State("Corner", team, 1),
                    new State("Out", team, 2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
        }
        return stage;
    };

    this.getNextState = function(stage){
        var possibleStates = [];
        // console.log(stage);
        stage.next.forEach(function(state){
            for(var i = 0; i < state.chance; i++){
                possibleStates.push(state);
            }
        });
        // console.log(possibleStates);
        var randomIndex = Math.round(Math.random()*(possibleStates.length-1));
        return possibleStates[randomIndex];
    };

    this.gameProtocol = function(){
        // array to hold all stages
        var stages = [];
        // coin toss for first half
        var coin = Math.round(Math.random());

        // first half
        stages.push({name: "First half", time: "FH"});
        var stage = this.getStage("Control", coin);
        var partsFH = 47;
        for(var i = 0; i < partsFH; i++){
            var minute = i;
            if(minute === (partsFH - 1) && (stage.name === "Penalty" || stage.name === "Shot" || stage.name === "Hand" || stage.name === "Free kick" || stage.name === "Cross" || stage.name === "Header")){
                partsFH++;
            }
            if(stage.name === "Goal"){
                stage.scorerFrom = stage.team;
            }
            if(stage.name === "Hand"){
                var chance = Math.random();
                if(chance < 0.1){
                    stage.redCardFor = stage.team;
                }
                if(chance > 0.4) {
                    stage.yellowCardFor = stage.team;
                }
            }
            if(stage.name === "Fouled"){
                var chance = Math.random();
                if(chance < 0.03) {
                    stage.redCardFor = 1-stage.team;
                }
                if(chance > 0.8){
                    stage.yellowCardFor = 1-stage.team;
                }
            }
            if(stage.name === "Free kick"){
                var chance = Math.random();
                if(chance < 0.03) {
                    stage.redCardFor = 1-stage.team;
                }
                if(chance > 0.7){
                    stage.yellowCardFor = 1-stage.team;
                }
            }
            if(i > 45) minute = "45 + " + (minute-45);
            stage.time = minute + "'";
            stages.push(stage);
            var nextState = this.getNextState(stage);
            stage = this.getStage(nextState.name, nextState.team);
        }
        stages.push({name: "Second half", time: "HT"});
        // second half
        var stage = this.getStage("Control", 1-coin);
        var partsSH = 94;
        for(var i = 45; i < partsSH; i++){
            var minute = i;
            if(minute === (partsSH - 1) && (stage.name === "Penalty" || stage.name === "Shot" || stage.name === "Hand" || stage.name === "Free kick" || stage.name === "Cross" || stage.name === "Header")){
                partsSH++;
            }
            if(i > 90) minute = "90 + " + (i - 90);
            if(stage.name === "Goal"){
                stage.scorerFrom = stage.team;
            }
            if(stage.name === "Hand"){
                var chance = Math.random();
                if(chance < 0.1){
                    stage.redCardFor = stage.team;
                }
                if(chance > 0.4) {
                    stage.yellowCardFor = stage.team;
                }
            }
            if(stage.name === "Fouled"){
                var chance = Math.random();
                if(chance < 0.03) {
                    stage.redCardFor = 1-stage.team;
                }
                if(chance > 0.8){
                    stage.yellowCardFor = 1-stage.team;
                }
            }
            if(stage.name === "Free kick"){
                var chance = Math.random();
                if(chance < 0.03) {
                    stage.redCardFor = 1-stage.team;
                }
                if(chance > 0.7){
                    stage.yellowCardFor = 1-stage.team;
                }
            }
            stage.time = minute + "'";
            stages.push(stage);
            var nextState = this.getNextState(stage);
            stage = this.getStage(nextState.name, nextState.team);
        }
        stages.push({name: "End of game", time: "FT"});

        var subsA = [];
        var numberOfSubsA = Math.round(Math.random()*2)+1;
        for(var i = 0; i < numberOfSubsA; i++){
            subsA.push(Math.round(Math.random()*45)+48);
        }

        var subsB = [];
        var numberOfSubsB = Math.round(Math.random()*2)+1;
        for(var i = 0; i < numberOfSubsB; i++){
            subsB.push(Math.round(Math.random()*45)+48);
        }

        subsA.forEach(function(s){
           stages[s].subA = 1;
        });

        subsB.forEach(function(s){
            stages[s].subB = 1;
        });



        return stages;
    };

};


$(".selectpicker").change(function(){
    var home = $("#sim-home").find(":selected").text();
    var away = $("#sim-away").find(":selected").text();

    if(home === away){
        $("#playbutton").attr("disabled", true);
    } else {
        $("#playbutton").attr("disabled", false);
    }
});

$("#playbutton").click(function() {
    $.ajax({
        type: "GET",
        url: "php/controller.php",
        data: {
            action: "getTeams",
            home: $("#sim-home").find(":selected").text(),
            away: $("#sim-away").find(":selected").text(),
            time: $("#sim-time").find(":selected").text()
        },
        success: function(msg){
            var obj = $.parseJSON(msg);
            $("#sim-input").hide();
            $("#sim-sim").show();
            visualise(obj);
        }
    });
});

var visualise = function(data){
    $("#logger").show();
    var time = data[2].time;
    var duration = 90;
    if(time === "Fast") duration = 10;

    var minutes = $("#game-minutes");
    var state = $("#game-state");
    var home_shots = $("#game-home-shots");
    var away_shots = $("#game-away-shots");
    var home_corners = $("#game-home-corners");
    var away_corners = $("#game-away-corners");
    var home_pos = $("#game-home-pos");
    var away_pos = $("#game-away-pos");

    var playersA = data[0].home_lineup.split(",");
    var benchA = data[0].home_bench.split(",");
    var powerA = parseInt(data[0].home_power);
    var nameA = data[0].home_name;
    var primaryA = data[0].home_primary;
    var secondaryA = data[0].home_secondary;
    var teamA = new Team(nameA, playersA, benchA, powerA, primaryA, secondaryA);

    var playersB = data[1].away_lineup.split(",");
    var benchB = data[1].away_bench.split(",");
    var powerB = parseInt(data[1].away_power);
    var nameB = data[1].away_name;
    var primaryB = data[1].away_primary;
    var secondaryB = data[1].away_secondary;
    var teamB = new Team(nameB, playersB, benchB, powerB, primaryB, secondaryB);

    $("#home-flag").addClass("flag-icon flag-icon-" + data[0].home_alpha);
    $("#away-flag").addClass("flag-icon flag-icon-" + data[1].away_alpha);

    $("#game-home").html(" " + nameA);
    $("#game-away").html(nameB + " ");

    var game = new Game(teamA, teamB);
    var ss = new StateSimulator(game);
    var protocol = ss.gameProtocol();

    var count = 0;

    var loop = function(){
        if(count === protocol.length) return;
        update(protocol[count]);
        count++;
        window.setTimeout(loop, duration*12);
    };

    var update = function(stage){
        if(stage.effect){
            stage.effect.call(game);
        }

        var possessionA = Math.round(ss.game.possessionParts[0]*100/(ss.game.possessionParts[0] + ss.game.possessionParts[1]));

        $("#game-home-result").html(ss.game.result[0]);
        $("#game-away-result").html(ss.game.result[1]);
        $("#game-home-shots").html(ss.game.shots[0]);
        $("#game-away-shots").html(ss.game.shots[1]);
        $("#game-home-corners").html(ss.game.corners[0]);
        $("#game-away-corners").html(ss.game.corners[1]);
        $("#game-home-pos").css("width", possessionA + "%");
        $("#game-away-pos").css("width", (100-possessionA) + "%");
        $("#game-home-pos").html(possessionA);
        $("#game-away-pos").html(100-possessionA);
        $("#game-minutes").css("width", 1.05*count + "%");
        $("#game-minutes").html(stage.time);

        if(ss.game.teams[stage.team]){
            var primary = ss.game.teams[stage.team].primaryColor;
            var secondary = ss.game.teams[stage.team].secondaryColor;
            $("#game-state").css("background-color", primary);
            $("#game-state").css("color", secondary);
        } else {
            $("#game-state").css("background-color", "#C7C9C9");
            $("#game-state").css("color", "#505354");
        }
        $("#game-state").html(stage.name);


        var formattedMinute = " " + stage.time + " ";
        if(stage.yellowCardFor > -1){
            var from = stage.yellowCardFor;
            var team = ss.game.teams[from];
            var player = team.markYellow();
            var card = player[1];

            var align = from === 0 ? "left" : "right";
            var yellowCardIcon = "<img src='flags/yellow.svg' style='width: 16px'>";
            var redCardIcon = "<img src='flags/red.svg' style='width: 16px'>";
            var icon = card === "yellow" ? yellowCardIcon : redCardIcon;
            var order = from === 0 ? (formattedMinute + icon + " " + player[0]) : (player[0] + " " + icon + formattedMinute);
            $("#log-info").append("<tr><td style='text-align:" + align + "'>" + order + "</td></tr>");

        }

        if(stage.redCardFor > -1){
            var from = stage.redCardFor;
            var team = ss.game.teams[from];
            var player = team.markRed();
            var card = player[1];

            var align = from === 0 ? "left" : "right";
            var redCardIcon = "<img src='flags/red.svg' style='width: 16px'>";
            var order = from === 0 ? (formattedMinute + redCardIcon + " " + player[0]) : (player[0] + " " + redCardIcon + formattedMinute);
            $("#log-info").append("<tr><td style='text-align:" + align + "'>" + order + "</td></tr>");
        }

        if(stage.scorerFrom > -1){
            var from = stage.scorerFrom;
            var team = ss.game.teams[from];
            var player = team.getScorer();

            var align = from === 0 ? "left" : "right";
            var ballIcon = "<img src='flags/soccer.svg' style='width: 16px'>"
            var order = from === 0 ? (formattedMinute + ballIcon + " " + player) : (player + " " + ballIcon + formattedMinute);
            $("#log-info").append("<tr><td style='text-align:" + align + "'>" + order + "</td></tr>");
        }

        var inIcon = "<img src='flags/in.svg' style='width: 16px'>";
        var offIcon = "<img src='flags/off.svg' style='width: 16px'>";

        if(stage.subA){
            var team = ss.game.teams[0];
            var players = team.substitute();

            $("#log-info").append("<tr><td style='text-align: left'>" + formattedMinute + offIcon + players[0] + inIcon + players[1] + "</td></tr>");
        }

        if(stage.subB){
            var team = ss.game.teams[1];
            var players = team.substitute();

            $("#log-info").append("<tr><td style='text-align: right'>" + players[1] + inIcon + players[0] + offIcon + formattedMinute + "</td></tr>");
        }
    };

    loop();
    

}
























