var Team = function(name, players, bench, power, primaryColor){
    this.name = name;
    this.players = players;
    this.bench = bench;
    this.power = power;
    if(!primaryColor){
        primaryColor = "444444";
    }
    this.primaryColor = primaryColor;

    this.cautioned = [];

    this.substitute = function(){
        var playerIndex = Math.floor(Math.random()*(this.players.length-1)+1);
        var benchIndex = Math.round(Math.floor(Math.random()*(this.bench.length-1)));
        var playerOff = this.players[playerIndex];
        var playerIn = this.bench[benchIndex];
        this.players[playerIndex] = playerIn;
        delete this.bench[benchIndex];
        return [playerOff, playerIn];
    };

    this.getScorer = function(){
        var count = 0;
        var chance = 0;
        var possibleScorers = [];
        this.players.forEach(function(p){
            if(count > 0) chance = 1;
            if(count > 4) chance = 2;
            if(count > 8) chance = 5;
            for(var i = 0; i < chance; i++){
                possibleScorers.push(p);
            }
            count++;
        });
        return possibleScorers[Math.floor(Math.random()*22)];
    };

    this.markYellow = function(){
        var playerIndex = Math.floor(Math.random()*(this.players.length-1))+1;
        var player = this.players[playerIndex];
        if(this.cautioned.indexOf(player) >= 0){
            delete this.players[playerIndex];
            return [player, "red"];
        }
        this.cautioned.push(player);
        return [player, "yellow"];
    };

    this.markRed = function(){
        var playerIndex = Math.floor(Math.random()*(this.players.length-1))+1;
        var player = this.players[playerIndex];
        delete this.players[playerIndex];
        return [player, "red"];
    };

};

var Game = function(home, away){
    this.home = home;
    this.away = away;
    this.teams = [home, away];

    this.result = [0,0];
    this.possessionParts = [0,0];
    this.shots = [0,0];
    this.corners = [0,0];
    this.chances = [0,0];

};

var StateSimulator = function(game){
    this.game = game;
    this.chances = [];
    this.chances[0] = calculateChances()[0];
    this.chances[1] = calculateChances()[1];
    console.log(this.chances[0], "   ----   ", this.chances[1]);

    function calculateChances(){
        var sum = game.home.power + game.away.power;
        return [Math.round(game.home.power*10/sum), Math.round(game.away.power*10/sum)];
    }

    function State(name, team, chance){
        this.name = name;
        this.team = team;
        this.chance = chance;
    }

    this.getStage = function(name, team){
        var stage = {};
        stage.name = name;
        stage.team = team;

        switch(name){
            case "control":
                stage.next =   [new State("attack", team, this.chances[team]*6),
                                new State("fouled", team, 2),
                                new State("attack", 1-team, this.chances[1-team]),
                                new State("fouled", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "attack":
                stage.next =   [new State("control", team, 5),
                                new State("shot", team, this.chances[team]*3),
                                new State("cross", team, this.chances[team]*2),
                                new State("offside", team, 2),
                                new State("win_ball", 1-team, this.chances[1-team]),
                                new State("free_kick", team, 7),
                                new State("fouled", team, 3),
                                new State("fouled", 1-team, 1),
                                new State("clearance", 1-team, this.chances[1-team])];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "counter_attack":
                stage.next =   [new State("penalty", team, 2),
                                new State("shot", team, this.chances[team]*4),
                                new State("win_ball", 1-team, this.chances[1-team]),
                                new State("attack", team, 4),
                                new State("cross", team, this.chances[team]*2),
                                new State("offside", team, 5),
                                new State("free_kick", team, 7),
                                new State("clearance", 1-team, this.chances[1-team]*2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "shot":
                stage.next =   [new State("block", 1-team, 15),
                                new State("save", 1-team, 15),
                                new State("corner", team, 15),
                                new State("out", team, 15),
                                new State("goal", team, this.chances[team]),
                                new State("hand", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
            case "corner":
                stage.next =   [new State("shot", team, this.chances[team]),
                                new State("clearance", 1-team, this.chances[1-team]),
                                new State("penalty", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.corners[team]++;
                };
                break;
            case "win_ball":
                stage.next =   [new State("attack", team, 2),
                                new State("counter_attack", team, 3),
                                new State("control", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "clearance":
                stage.next =   [new State("attack", 1-team, this.chances[1-team]),
                                new State("control", 1-team, this.chances[1-team]),
                                new State("counter_attack", team, this.chances[team]),
                                new State("attack", team, this.chances[team])];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "block":
                stage.next =   [new State("attack", 1-team, this.chances[1-team]*2),
                                new State("attack", team, this.chances[team]*3),
                                new State("counter_attack", team, this.chances[team]*2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "save":
                stage.next =   [new State("attack", team, 4),
                                new State("counter_attack", team, this.chances[team]*2),
                                new State("control", team, 3)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "hand":
                stage.next =   [new State("penalty", 1-team, 1)];stage.effect = function(){
                this.possessionParts[1-team]++;
            };
                break;
            case "goal":
                stage.next =   [new State("control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.result[team]++;
                };
                break;
            case "out":
                stage.next =   [new State("attack", 1-team, 3),
                                new State("control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[1-team]++;
                };
                break;
            case "offside":
                stage.next =   [new State("attack", 1-team, 3),
                                new State("control", 1-team, 1)];
                stage.effect = function(){
                    this.possessionParts[1-team]++;
                };
                break;
            case "fouled":
                stage.next =   [new State("attack", team, 3),
                                new State("control", team, 1)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "free_kick":
                stage.next =   [new State("cross", team, 4),
                                new State("goal", team, this.chances[team]),
                                new State("block", 1-team, this.chances[1-team]),
                                new State("out", team, 12),
                                new State("penalty", team, 2),
                                new State("corner", team, 11)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
            case "cross":
                stage.next =   [new State("offside", team, 7),
                                new State("penalty", team, 1),
                                new State("fouled", 1-team, 3),
                                new State("win_ball", 1-team, this.chances[1-team]*2),
                                new State("clearance", 1-team, this.chances[1-team]*2),
                                new State("shot", team, this.chances[team]*2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                };
                break;
            case "penalty":
                stage.next =   [new State("goal", team, 21),
                                new State("save", 1-team, 3),
                                new State("corner", team, 1),
                                new State("out", team, 2)];
                stage.effect = function(){
                    this.possessionParts[team]++;
                    this.shots[team]++;
                };
                break;
        }
        return stage;
    };

    this.getNextState = function(stage){
        var possibleStates = [];
        // console.log(stage);
        stage.next.forEach(function(state){
            for(var i = 0; i < state.chance; i++){
                possibleStates.push(state);
            }
        });
        // console.log(possibleStates);
        var randomIndex = Math.round(Math.random()*(possibleStates.length-1));
        return possibleStates[randomIndex];
    };

    this.gameProtocol = function(){
        // array to hold all stages
        var stages = [];
        // coin toss for first half
        var coin = Math.round(Math.random());

        // first half
        stages.push({name: "First half", time: ""});
        var stage = this.getStage("control", coin);
        for(var i = 0; i < 47; i++){
            var minute = i;
            if(i > 45) minute = "45 + 1";
            stage.time = minute + "";
            stages.push(stage);
            var nextState = this.getNextState(stage);
            stage = this.getStage(nextState.name, nextState.team);
        }
        stages.push({name: "Second half", time: ""});
        // second half
        var stage = this.getStage("control", 1-coin);
        for(var i = 45; i < 94; i++){
            var minute = i;
            if(i > 90) minute = "90 + " + (i - 90);
            stage.time = minute + "";
            stages.push(stage);
            var nextState = this.getNextState(stage);
            stage = this.getStage(nextState.name, nextState.team);
        }
        stages.push({name: "End of game", time: ""});

        return stages;
    };

};

visualize();

function visualize(){
    var minute = document.getElementById("minute");
    var state = document.getElementById("state");
    var home = document.getElementById("home");
    var away = document.getElementById("away");


    var playersA = ["Neuer", "Boateng", "Howedes", "Hummels", "Kimmich", "Kroos", "Gotze", "Schweinsteiger", "Draxler", "Muller", "Gomez"];
    var benchA = ["Hector", "Mustafi", "Khedira", "Ozil", "Schurrle", "Podolski"];
    var powerA = 10;
    var germany = new Team("Germany", playersA, benchA, powerA, "#8F8759");

    var playersB = ["Lloris", "Evra", "Koscielny", "Mangala", "Sagna", "Cabaye", "Coman", "Pogba", "Sissoko", "Giroud", "Griezmann"];
    var benchB = ["Gignac", "Schneiderlin", "Matuidi", "Zidane", "Benzema", "Jallet"];
    var powerB = 10;
    var france = new Team("France", playersB, benchB, powerB, "#3262A6");
    var game = new Game(germany, france);
    var ss = new StateSimulator(game);
    var protocol = ss.gameProtocol();
    var count = 0;

    var loop = function(){
        if(count === protocol.length) return;
        update(protocol[count]);
        count++;
        window.setTimeout(loop, 10);
    };

    var update = function(stage){
        if(stage.effect){
            stage.effect.call(game);
        }
        minute.innerHTML = stage.time;
        state.innerHTML = stage.name;

        if(ss.game.teams[stage.team]){
            var current = ss.game.teams[stage.team].primaryColor;
            state.style.color = current;
        }

        var possessionA = Math.round(ss.game.possessionParts[0]*100/(ss.game.possessionParts[0] + ss.game.possessionParts[1]));
        home.innerHTML = ss.game.teams[0].name + " " + ss.game.result[0] + "  shots: " + ss.game.shots[0] + "   possesion " + possessionA;
        away.innerHTML = ss.game.teams[1].name + " " + ss.game.result[1] + "  shots: " + ss.game.shots[1] + "   possesion " + (100-possessionA);
    };

    loop();
}




