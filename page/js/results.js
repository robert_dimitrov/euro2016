$("#add-result").click(function(){

    $.ajax({
        type: "POST",
        url: "php/controller.php",
        data: {
            action: "addResult",
            game_id: $("#result-game-id").val(),
            result: $("#result-result").val(),
            outcome: getOutcome($("#result-result").val())
        },
        success: function(json){
            console.log(json);
        }
    });
});

var getOutcome = function(result){
    var goalsB = parseInt(result.substring(2,3));
    var goalsA = parseInt(result.substring(0,1));
    if(goalsA > goalsB) return "1";
    if(goalsA < goalsB) return "2";
    if(goalsA === goalsB) return "X";
};














$(window).load(function() {
    $.ajax({
        type: "GET",
        url: "php/controller.php",
        data: {
            action: "getActiveBets"
        },
        success: function(msg){
            // console.log(msg);
            var obj = $.parseJSON(msg);
            var res= "";
            var lastdate = 0;
            $.each(obj, function(index, value){
                // var betdateMysql = new Date(Date.parse(value.date.replace('-','/','g')));
                var t = value.date.split(/[- :]/);
                var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                var betdate =  d.getDate() + "." + parseInt(d.getMonth()+1);
                if(betdate === lastdate){
                    res += createGameRow(value, true);
                } else {
                    res += createGameRow(value, true, betdate);
                    lastdate = betdate;
                }
            });
            $("#bets-content").html(res);
        }
    });
});

var createGameRow = function(info, button, newdate){
    var content = "";
    // var gameDate = new Date(Date.parse(info.date.replace('-','/','g')));
    console.log(info);
    var t = info.date.split(/[- :]/);
    var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
    var hour = "" + ("0" + (d.getHours())).slice(-2) + ":" + ("0" + (d.getMinutes())).slice(-2);
    if(newdate){
        content += "<h4 class='game-date'>" + newdate + "</h4>";
    }
    var scoreHome = info.result.substring(0,1);
    var scoreAway = info.result.substring(2);
    var betable = "";
    var buttonName = "Bet";
    var buttonClass = "btn btn-success placebet";
    var buttonClickable = " disabled ";
    if(button && info.bet){
        scoreHome = info.bet.substring(0,1);
        scoreAway = info.bet.substring(2,3);
        betable = " disabled ";
        buttonName = "Edit";
        buttonClass = "btn btn-primary editbet";
        buttonClickable = "";
    }

    content += "<div class='row'>" +
        "<div class='col-lg-6'>" +
        "<div class='input-group'>" +
        "<span class='input-group-addon'>" + hour +  "</span>" +
        "<span class='input-group-addon teamname'><span class='flag-icon flag-icon-" + info.home_alpha2 + "'></span> " + info.home_name + "</span>" +
        "<input type='text' class='form-control betinput' id='" + "home-goals-" + info.id + "' size='1' " + (button ? "" : " disabled ") + " onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength='1' " + betable + " value='" + scoreHome +  "'/>" +
        "<span class='input-group-addon' style='border-left: 0; border-right: 0;'>:</span>" +
        "<input type='text' class='form-control betinput' id='" + "away-goals-" + info.id + "' size='1' " + (button ? "" : " disabled ") + " onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength='1' " + betable + " value='" + scoreAway + "'/>" +
        "<span class='input-group-addon teamname'>" + info.away_name + " <span class='flag-icon flag-icon-" + info.away_alpha2 + "'></span> </span>" + (button ?
        "<span id='" + "bet-" + info.id + "' class='input-group-addon " + buttonClass + "'" + buttonClickable + ">" + buttonName + "</span>" : "") +
        "</div>" +
        "</div>" +
        "<div class='col-lg-3'></div></div><br>";
    return content;
};


var getResult = function(home, away){
    return home+"-"+away;
};



$(window).load(function(){
    $.ajax({
        type: "GET",
        url: "php/controller.php",
        data: {
            action: "getResults"
        },
        success: function(msg){
            // console.log("---old games ---", msg);
            var obj = $.parseJSON(msg);
            var res= "";
            var lastdate = 0;
            $.each(obj, function(index, value){
                // var betdateMysql = new Date(Date.parse(value.date.replace('-','/','g')));
                var table = "";
                var t = value.date.split(/[- :]/);
                var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                var betdate =  d.getDate() + "." + parseInt(d.getMonth()+1);
                if(betdate === lastdate){
                    res += createGameRow(value, false);
                } else {
                    res += createGameRow(value, false, betdate);
                    lastdate = betdate;
                }
                table += "<div class='row'><div class='col-lg-6'><table id='placed-" + value.id +  "' class='table table-condensed table-hover table-placedbets table-standings'><thead><tr><td>Name</td><td>Bet</td><td>Points</td></tr></thead><tbody></tbody></table></div></div><br>";
                res += table;
                visualizeBet(value);
            });
            $("#results-content").html(res);
        }
    });

});

var visualizeBet = function(bet){
    $.ajax({
        type: "GET",
        url: "php/controller.php",
        data: {
            action: "getResultsToBet",
            game_id: bet.id
        },
        success: function(msg){
            var obj = $.parseJSON(msg);
            var res= "";
            var lastdate = 0;
            $.each(obj, function(index, value){
                $("#placed-" + bet.id + " tbody").append("<tr><td>" + value.display_name + "</td><td>" + value.bet_result + "</td><td class='points-" + value.points + "'>" + value.points + " </td></tr>");
            });
        }
    });
};