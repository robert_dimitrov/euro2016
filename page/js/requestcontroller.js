$(function() {

    $("#getAllActive").click(function() {
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getActiveBets"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var res= "";
                $.each(obj, function(index, value){
                    res += "<p>" + value.home_name + "</p>";
                });
                $("#activeBets").html(res);
            }
        });
    });

    $("#getOldBets").click(function() {
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getOldBets"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var res= "";
                $.each(obj, function(index, value){
                    res += "<p>" + value.home_name + "</p>";
                });
                $("#oldBets").html(res);
            }
        });
    });

    $("#getPlacedBets").click(function() {
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getPlacedBets"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var res= "";
                $.each(obj, function(index, value){
                    res += "<p>" + value.home_name + "</p>";
                });
                $("#placedBets").html(res);
            }
        });
    });

    $("#getResults").click(function() {
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getResults"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var res= "";
                $.each(obj, function(index, value){
                    res += "<p>" + value.date + "   -- " + value.home_name + "</p>" + "<p id="+value.game_id+"></p>";
                    getBetsToFinishedGame(value.game_id);
                });
                $("#results").html(res);
            }
        });
    });

    var getBetsToFinishedGame = function(id){
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getResultsToBet",
                game_id: id
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var res= "";
                $.each(obj, function(index, value){
                    res += value.bet_result;
                });
                $("#" + id).html(res);
            }
        });
    };

    $("#std").click(function() {
        $.ajax({
            type: "GET",
            url: "php/controller.php",
            data: {
                action: "getStandings"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
                var users = [];
                $.each(obj, function(index, value){
                    users.push(value);
                });
                users.sort(function(a,b){
                    var aPoints = parseInt(a.points);
                    var bPoints = parseInt(b.points);
                    if(aPoints !== bPoints) return bPoints - aPoints;
                    return parseInt(b.perfect) - parseInt(a.perfect);
                });
                var position = 1;
                var res = "<table class='table table-condensed table-hover table-standings'><thead><tr><td>#</td><td>Name</td><td>Bets</td><td>Perfect</td><td>Points</td></tr></thead><tbody>";
                users.forEach(function(u){
                    res += "<tr><td>" + position + "</td><td>" + u.name + "</td><td>" + u.count + "</td><td>" + u.perfect + "</td><td class='points-standings'>" + u.points + "</td></tr>";
                    position++;
                });
                res += "</tbody></table>";
                $("#standings").html(res);
            }
        });
    });

    $("#getTeams").click(function() {
        $.ajax({
            type: "GET",
            url: "../php/controller.php",
            data: {
                action: "getTeams",
                home: "Belgium",
                away: "Croatia"
            },
            success: function(msg){
                var obj = $.parseJSON(msg);
            }
        });
    });

    $("#placeBet").click(function(){
        $.ajax({
            type: "POST",
            url: "../php/controller.php",
            data: {
                action: "placeBet",
                game_id: "104",
                result: "7-2",
                outcome: getOutcome("7-2")
            },
            success: function(json){
            }
        });
    });

    var getOutcome = function(result){
        var goalsB = parseInt(result.substring(2,3));
        var goalsA = parseInt(result.substring(0,1));
        if(goalsA > goalsB) return "1";
        if(goalsA < goalsB) return "2";
        if(goalsA === goalsB) return "X";
    };

    $("#addResult").click(function(){

        $.ajax({
            type: "POST",
            url: "../php/controller.php",
            data: {
                action: "addResult",
                game_id: "100",
                result: "0-2",
                outcome: getOutcome("0-2")
            },
            success: function(json){
            }
        });
    });





});