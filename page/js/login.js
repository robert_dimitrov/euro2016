$(".form-control").keyup(function(){
    var username = $("#username");
    var password = $("#password");

    // var empty = invitation.val() === "" || username.val() === "" || password.val() === "" || display_name.val() === "";
    var empty = username.val().length < 1 || password.val().length < 1;

    var valid = username.val().match(username.attr("pattern")) &&
        password.val().match(password.attr("pattern"));
    $("#signupbutton").attr("disabled", !valid || empty);
});