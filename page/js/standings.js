$(window).load(function() {
    $.ajax({
        type: "GET",
        url: "php/controller.php",
        data: {
            action: "getStandings"
        },
        success: function(msg){
            console.log("---------   " + msg);
            var obj = $.parseJSON(msg);
            console.log(msg.length + "  len");
            console.log("json_" + obj);
            var users = [];
            $.each(obj, function(index, value){
                users.push(value);
            });
            users.sort(function(a,b){
                var aPoints = parseInt(a.points);
                var bPoints = parseInt(b.points);
                if(aPoints !== bPoints) return bPoints - aPoints;
                var aPerfect = parseInt(a.perfect);
                var bPerfect = parseInt(b.perfect);
                if(aPerfect !== bPerfect) return bPerfect - aPerfect;
                return parseInt(a.count) - parseInt(b.count);
            });
            var position = 1;
            var res = "<table class='table table-condensed table-hover table-standings'><thead><tr><td>#</td><td>Name</td><td>Bets</td><td>Good</td><td>Perfect</td><td>Points</td></tr></thead><tbody>";
            users.forEach(function(u){
                res += "<tr><td>" + position + "</td><td>" + u.name + "</td><td>" + u.count + "</td><td>" + u.good + "</td><td>" + u.perfect + "</td><td class='points-standings'>" + u.points + "</td></tr>";
                position++;
            });
            res += "</tbody></table>";
            $("#standings").html(res);
        }
    });
});