<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Bets" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Bets
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <?php
                if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                    echo '<ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#active-bets" id="tab-active">Active Bets</a></li>
                                            <li><a data-toggle="tab" href="#placed-bets" id="tab-placed">Placed Bets</a></li>
                                          </ul>

                                          <br>




                                          <div class="row" id="alert-active">
                                              <div class="col-lg-12">
                                                  <div class="alert alert-warning alert-dismissable">
                                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                      <i class="fa fa-info-circle"></i>  The 1/8-final matches (starting this saturday) will be uploaded on saturday morning due to necessary changes on the database system.
                                                  </div>
                                              </div>
                                              <div class="col-lg-12">
                                                    <div class="alert alert-info alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                        <i class="fa fa-info-circle"></i>  You can place (OR EDIT) a bet up to 10 minutes before the official start of a game.
                                                    </div>
                                                </div>

                                          </div>

                                          <div class="row" id="alert-placed" hidden>
                                              <div class="col-lg-12">
                                                  <div class="alert alert-info alert-dismissable">
                                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                      <i class="fa fa-info-circle"></i>  You can see all the bets to a game 10 minutes before its start.
                                                  </div>
                                              </div>
                                          </div>


                                          <div class="tab-content">
                                            <div id="active-bets" class="tab-pane fade in active">
                                              <div id="bets-content">
                                              <br>
                                              </div>
                                           </div>

                                           <div class="tab-content">
                                             <div id="placed-bets" class="tab-pane fade in active">
                                               <div id="placed-bets-content" hidden>
                                               <br>
                                               </div>
                                            </div>




                                          <!-- /.row -->
                                          </div>
                                          <!-- /.row -->';
                } else {
                    echo '<div class="row" id="alert-active">
                                                                        <div class="col-lg-12">
                                                                            <div class="alert alert-danger alert-dismissable">
                                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                                                <i class="fa fa-info-circle"></i>  Placing a bet requires that you are logged-in.
                                                                            </div>
                                                                        </div>';
                }
                ?>




            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>








<?php include 'scripts.php'?>
<script src="js/bets.js"></script>
</body>
</html>