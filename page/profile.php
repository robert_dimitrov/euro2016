<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Profile" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" id="standingspage">
                        <h1 class="page-header">
                            Profile
                        </h1>
                    </div>
                </div>

                <?php
                if(isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                  echo '<div class="row" id="alert-active">
                                       <div class="col-lg-12">
                                           <div class="alert alert-info alert-dismissable">
                                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                               <i class="fa fa-info-circle"></i> Currently working on this one, please have patience.
                                           </div>
                                       </div>';
                } else {
                  echo '<div class="row" id="alert-active">
                      <div class="col-lg-12">
                          <div class="alert alert-danger alert-dismissable">
                              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              <i class="fa fa-info-circle"></i>  Please log in.
                          </div>
                      </div>';
                }
                ?>





            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>



<?php include 'scripts.php'?>
</body>
</html>