<?php
    session_start();
?>


<!DOCTYPE html>
<html lang="en">
<?php $title = "EM-Bet 2016 - Simulator" ?>
<?php include 'head.php'?>

<body>
<?php include 'navigation.php'?>

    <div id="wrapper">


        <div id="page-wrapper">

            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12" id="standingspage">
                        <h1 class="page-header">
                            Simulator
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
                <br>



                <div class="row" id="sim-input">
                <div class="col-lg-2">
                </div>

                <div class="col-lg-8">
                <div class="input-group ">


                  <select class="selectpicker" data-live-search="true" id="sim-home">
                      <option data-tokens="Albania" data-content="<span class='flag-icon flag-icon-al'></span><span> Albania</span>">Albania</option>
                      <option data-tokens="Austria" data-content="<span class='flag-icon flag-icon-at'></span><span> Austria</span>">Austria</option>
                      <option data-tokens="Belgium" data-content="<span class='flag-icon flag-icon-be'></span><span> Belgium</span>">Belgium</option>
                      <option data-tokens="Croatia" data-content="<span class='flag-icon flag-icon-hr'></span><span> Croatia</span>">Croatia</option>
                      <option data-tokens="Czech" data-content="<span class='flag-icon flag-icon-cz'></span><span> Czech</span>">Czech</option>
                      <option data-tokens="England" data-content="<span class='flag-icon flag-icon-gb-eng'></span><span> England</span>">England</option>
                      <option data-tokens="France"  selected data-content="<span class='flag-icon flag-icon-fr'></span><span> France</span>">France</option>
                      <option data-tokens="Germany" data-content="<span class='flag-icon flag-icon-de'></span><span> Germany</span>">Germany</option>
                      <option data-tokens="Hungary" data-content="<span class='flag-icon flag-icon-hu'></span><span> Hungary</span>">Hungary</option>
                      <option data-tokens="Iceland" data-content="<span class='flag-icon flag-icon-is'></span><span> Iceland</span>">Iceland</option>
                                            <option data-tokens="Ireland" data-content="<span class='flag-icon flag-icon-ie'></span><span> Ireland</span>">Ireland</option>
                      <option data-tokens="Italy" data-content="<span class='flag-icon flag-icon-it'></span><span> Italy</span>">Italy</option>
                      <option data-tokens="N. Ireland" data-content="<span class='flag-icon flag-icon-gb-nir'></span><span> N. Ireland</span>">N. Ireland</option>
                      <option data-tokens="Poland" data-content="<span class='flag-icon flag-icon-pl'></span><span> Poland</span>">Poland</option>
                      <option data-tokens="Portugal" data-content="<span class='flag-icon flag-icon-pt'></span><span> Portugal</span>">Portugal</option>
                      <option data-tokens="Romania" data-content="<span class='flag-icon flag-icon-ro'></span><span> Romania</span>">Romania</option>
                      <option data-tokens="Russia" data-content="<span class='flag-icon flag-icon-ru'></span><span> Russia</span>">Russia</option>
                      <option data-tokens="Slovakia" data-content="<span class='flag-icon flag-icon-sk'></span><span> Slovakia</span>">Slovakia</option>
                      <option data-tokens="Spain" data-content="<span class='flag-icon flag-icon-es'></span><span> Spain</span>">Spain</option>
                      <option data-tokens="Sweden" data-content="<span class='flag-icon flag-icon-se'></span><span> Sweden</span>">Sweden</option>
                      <option data-tokens="Switzerland" data-content="<span class='flag-icon flag-icon-ch'></span><span> Switzerland</span>">Switzerland</option>
                      <option data-tokens="Turkey" data-content="<span class='flag-icon flag-icon-tr'></span><span> Turkey</span>">Turkey</option>
                      <option data-tokens="Ukraine" data-content="<span class='flag-icon flag-icon-ua'></span><span> Ukraine</span>">Ukraine</option>
                      <option data-tokens="Wales" data-content="<span class='flag-icon flag-icon-gb-wls'></span><span> Wales</span>">Wales</option>
                  </select>
                  <select class="selectpicker secondary-addon" id="sim-time">
                      <option>Fast</option>
                      <option selected>Slow</option>
                  </select>
                  <select class="selectpicker" data-live-search="true" id="sim-away">
                    <option data-tokens="Albania" data-content="<span class='flag-icon flag-icon-al'></span><span> Albania</span>">Albania</option>
                    <option data-tokens="Austria" data-content="<span class='flag-icon flag-icon-at'></span><span> Austria</span>">Austria</option>
                    <option data-tokens="Belgium" data-content="<span class='flag-icon flag-icon-be'></span><span> Belgium</span>">Belgium</option>
                    <option data-tokens="Croatia" data-content="<span class='flag-icon flag-icon-hr'></span><span> Croatia</span>">Croatia</option>
                    <option data-tokens="Czech" data-content="<span class='flag-icon flag-icon-cz'></span><span> Czech</span>">Czech</option>
                    <option data-tokens="England" data-content="<span class='flag-icon flag-icon-gb-eng'></span><span> England</span>">England</option>
                    <option data-tokens="France" data-content="<span class='flag-icon flag-icon-fr'></span><span> France</span>">France</option>
                    <option data-tokens="Germany"  data-content="<span class='flag-icon flag-icon-de'></span><span> Germany</span>">Germany</option>
                    <option data-tokens="Hungary" data-content="<span class='flag-icon flag-icon-hu'></span><span> Hungary</span>">Hungary</option>
                    <option data-tokens="Iceland" data-content="<span class='flag-icon flag-icon-is'></span><span> Iceland</span>">Iceland</option>
                                        <option data-tokens="Ireland" data-content="<span class='flag-icon flag-icon-ie'></span><span> Ireland</span>">Ireland</option>
                    <option data-tokens="Italy" data-content="<span class='flag-icon flag-icon-it'></span><span> Italy</span>">Italy</option>
                    <option data-tokens="N. Ireland" data-content="<span class='flag-icon flag-icon-gb-nir'></span><span> N. Ireland</span>">N. Ireland</option>
                    <option data-tokens="Poland" data-content="<span class='flag-icon flag-icon-pl'></span><span> Poland</span>">Poland</option>
                    <option data-tokens="Portugal" data-content="<span class='flag-icon flag-icon-pt'></span><span> Portugal</span>">Portugal</option>
                    <option data-tokens="Romania" selected data-content="<span class='flag-icon flag-icon-ro'></span><span> Romania</span>">Romania</option>
                    <option data-tokens="Russia" data-content="<span class='flag-icon flag-icon-ru'></span><span> Russia</span>">Russia</option>
                    <option data-tokens="Slovakia" data-content="<span class='flag-icon flag-icon-sk'></span><span> Slovakia</span>">Slovakia</option>
                    <option data-tokens="Spain" data-content="<span class='flag-icon flag-icon-es'></span><span> Spain</span>">Spain</option>
                    <option data-tokens="Sweden" data-content="<span class='flag-icon flag-icon-se'></span><span> Sweden</span>">Sweden</option>
                    <option data-tokens="Switzerland" data-content="<span class='flag-icon flag-icon-ch'></span><span> Switzerland</span>">Switzerland</option>
                    <option data-tokens="Turkey" data-content="<span class='flag-icon flag-icon-tr'></span><span> Turkey</span>">Turkey</option>
                    <option data-tokens="Ukraine" data-content="<span class='flag-icon flag-icon-ua'></span><span> Ukraine</span>">Ukraine</option>
                    <option data-tokens="Wales" data-content="<span class='flag-icon flag-icon-gb-wls'></span><span> Wales</span>">Wales</option>
                </select>
                <span class="input-group-btn">
                      <button id="playbutton" class="btn btn-success" type="button">PLAY</button>
              </span>

                </div>
                </div>
                <div class="col-lg-2">
                </div>
                <br><br>
                </div>




                <div id="sim-sim" hidden>
                <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                  <table class="table table-bordered table-hover table-condensed simtable">
                    <tbody>
                      <tr>
                        <td class="team"><span id="home-flag"></span><span id="game-home"></span></td>
                        <td  id="game-home-result"></td>
                        <td id="game-away-result"></td>
                        <td class="team"><span id="game-away"></span><span id="away-flag"></span></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>

                <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                <table class="table table-bordered table-hover table-condensed simtable">
                <tbody>
                <tr>
                <td>
                <div class="progress" style="margin-top: 20px">
                  <div class="progress-bar" role="progressbar"
                  aria-valuemin="0" aria-valuemax="100" style="width:0%" id="game-minutes">

                  </div>
                </div>
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>

                <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                <div class="well" id="game-state"></div>
                </div>
                </div>

                <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                  <table class="table table-bordered table-hover table-condensed simtable">
                    <tbody>
                      <tr>
                        <td id="game-home-shots"></td>
                        <td class="result">Shots</td>
                        <td id="game-away-shots"></td>
                      </tr>
                <tr>
                        <td id="game-home-corners"></td>
                        <td class="result">Corners</td>
                        <td id="game-away-corners"></td>
                      </tr>
                <tr>
                <tr>
                <td colspan="3">Posession</td>
                </tr>
                <td colspan="3"><div class="progress" style="margin-top: 20px">
                  <div class="progress">
                  <div class="progress-bar progress-bar-success custom" role="progressbar" style="width:50%" id="game-home-pos">

                  </div>
                  <div class="progress-bar progress-bar-warning bustom" role="progressbar" style="width:50%" id="game-away-pos">

                  </div></td>
                </tr>
                    </tbody>
                  </table>
                  </div></div>
                </div>

                <div class="row" id="logger">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                  <table class="table table-bordered table-hover table-condensed simtable">
                    <tbody id="log-info">

                    </tbody>
                  </table>
                </div>
                </div>






            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>



<?php include 'scripts.php'?>
<script src="js/sim-validation.js"></script>
</body>
</html>